/*
 * The MIT License
 *
 * Copyright 2015 David Knap.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cz.roleplaying.liana;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Defines application graphic interface.
 * @author David Knap
 */
public class MainWindow extends Application {
  
  private Stage stage;

  @Override
  public void start(Stage primaryStage) {
	
	stage = primaryStage;

	Scene scene = new Scene(new OnlineListScene(this).getRoot(), 200, 400);

	primaryStage.setTitle("Liana");
	primaryStage.setScene(scene);
	primaryStage.show();
  }
  
  public void pinToTop(boolean newState) {
	stage.setAlwaysOnTop(newState);
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
	launch(args);
  }

}
