/*
 * The MIT License
 *
 * Copyright 2015 David Knap.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cz.roleplaying.liana;

import cz.roleplaying.liana.model.PlayerCharacter;
import cz.roleplaying.liana.remote.RemoteService;
import cz.roleplaying.liana.remote.RemoteServiceFile;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/**
 * Defines online characters list view with updating.
 *
 * @author David Knap
 */
public final class OnlineListScene {

  private final MainWindow window;
  private final VBox root;
  private ToolBar mainToolbar;
  private final Button updateButton;
  private final Button autoUpdateButton;
  private final Button pinToTopButton;
  private boolean autoUpdateOn;
  private boolean pinToTopOn;
  private final TableView onlineCharactersList;
  private final Timeline updateCharacterListTask;
  private final FriendList friends;
  private final RemoteService dataProvider;
  private final ObservableList<PlayerCharacter> characters = FXCollections.observableArrayList();

  /**
   * Constructs online characters list view.
   * @param app Super window
   */
  public OnlineListScene(MainWindow app) {
	
	window = app;

	root = new VBox();
	mainToolbar = new ToolBar();
	updateButton = new Button(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("UPDATE_BUTTON_TEXT"), new Glyph("FontAwesome", FontAwesome.Glyph.REFRESH));
	updateButton.setTooltip(new Tooltip(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("UPDATE_BUTTON_TOOLTIP")));
	updateButton.setOnAction((ActionEvent event) -> {
	  updateCharacterList();
	});
	autoUpdateButton = new Button(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("AUTOUPDATE_BUTTON_TEXT"), new Glyph(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("FONTAWESOME"), FontAwesome.Glyph.REFRESH).color(Color.RED));
	autoUpdateButton.setTooltip(new Tooltip(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("AUTOUPDATE_BUTTON_TOOLTIP")));
	autoUpdateButton.setOnAction((ActionEvent event) -> {
	  toggleAutoUpdateCharacterList();
	});
	pinToTopButton = new Button(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("PINTOTOP_BUTTON_TEXT"), new Glyph(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("FONTAWESOME"), FontAwesome.Glyph.THUMB_TACK).color(Color.RED));
	pinToTopButton.setTooltip(new Tooltip(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("PINTOTOP_BUTTON_TOOLTIP")));
	pinToTopButton.setOnAction((ActionEvent event) -> {
	  togglePinToTop();
	});

	updateCharacterListTask = new Timeline(new KeyFrame(Duration.seconds(60), (ActionEvent event) -> {
	  updateCharacterList();
	}));
	updateCharacterListTask.setCycleCount(Timeline.INDEFINITE);

	friends = new FriendList(Paths.get("./liana-friends.conf"));
	dataProvider = new RemoteServiceFile(Paths.get("./liana-servers.conf"));

	mainToolbar = new ToolBar(updateButton, autoUpdateButton, pinToTopButton);

	onlineCharactersList = new TableView();
	TableColumn nameCol = new TableColumn(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("ONLINE_TABLE_COL_NAME"));
	TableColumn realmCol = new TableColumn(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("ONLINE_TABLE_COL_REALM"));
	nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
	realmCol.setCellValueFactory(new PropertyValueFactory<>("realm"));
	onlineCharactersList.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	onlineCharactersList.setPlaceholder(new Label(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("ONLINE_TABLE_PLACEHOLDER")));
	onlineCharactersList.getColumns().addAll(nameCol, realmCol);

	updateCharacterList();

	root.getChildren().addAll(mainToolbar, onlineCharactersList);
  }

  private void updateCharacterList() {

	Set<String> oldCharacters = new HashSet<>();
	for (PlayerCharacter character : characters) {
	  oldCharacters.add(character.getName());
	}

	characters.clear();

	List<String> friendsOnline = new ArrayList<>();
	
	for (PlayerCharacter character : dataProvider.getOnlineCharacters()) {
	  if (!(oldCharacters.contains(character.getName())) && friends.isFriend(character)) {
		  friendsOnline.add(character.getName());
		}
	  characters.add(character);
	}

	characters.sort((PlayerCharacter t, PlayerCharacter t1) -> t.getName().compareTo(t1.getName()));

	onlineCharactersList.setItems(characters);

	if (!friendsOnline.isEmpty()) {
	  Platform.runLater(() -> {
		Stage utility = new Stage(StageStyle.TRANSPARENT);
		StackPane rootSp = new StackPane();
		Scene scene = new Scene(rootSp, 1, 1);
		utility.setScene(scene);
		utility.toBack();
		utility.show();
		for (String friendOnline : friendsOnline) {
		  Notifications.create().title("Liana").text(friendOnline + java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("FRIEND_NOW_ONLINE")).hideAfter(Duration.seconds(4)).show();
		}
		PauseTransition utilityDelay = new PauseTransition(Duration.seconds(5));
		utilityDelay.setOnFinished(event -> utility.close());
		utilityDelay.play();
	  });
	}

	System.out.println(java.util.ResourceBundle.getBundle("cz/roleplaying/liana/Strings").getString("ONLINE_TABLE_LOG_UPDATED"));
  }

  private void enableAutoUpdateCharacterList() {
	updateCharacterListTask.play();
  }

  private void disableAutoUpdateCharacterList() {
	updateCharacterListTask.stop();
  }

  private void toggleAutoUpdateCharacterList() {
	if (autoUpdateOn) {
	  autoUpdateButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.REFRESH).color(Color.RED));
	  disableAutoUpdateCharacterList();
	  autoUpdateOn = false;
	} else {
	  autoUpdateButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.REFRESH).color(Color.GREEN));
	  enableAutoUpdateCharacterList();
	  autoUpdateOn = true;
	}
  }

  /**
   * Get whole view as single VBox with all elements in child hierarchy.
   *
   * @return Node representing whole view.
   */
  public VBox getRoot() {
	return root;
  }

  private void togglePinToTop() {
	if (pinToTopOn) {
	  pinToTopButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.THUMB_TACK).color(Color.RED));
	  window.pinToTop(false);
	  pinToTopOn = false;
	} else {
	  pinToTopButton.setGraphic(new Glyph("FontAwesome", FontAwesome.Glyph.THUMB_TACK).color(Color.GREEN));
	  window.pinToTop(true);
	  pinToTopOn = true;
	}
  }

}
