/*
 * The MIT License
 *
 * Copyright 2016 David Knap.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cz.roleplaying.liana.remote;

import cz.roleplaying.liana.model.PlayerCharacter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author David Knap
 * @version 1.0
 */
public class RemoteServiceFile implements RemoteService {
  
  Map<String, URL> providers = new HashMap<>();

  public RemoteServiceFile(Path file) {
	try (Stream<String> lines = Files.lines(file)) {
	  lines.forEach((String s) -> {
		if (!(s.trim().startsWith("#"))) {
		  String[] row = s.split("==");
		  String serverName = row[0].trim();
		  URL serverUrl;
		  try {
			serverUrl = new URL(row[1].trim());
			providers.put(serverName, serverUrl);
		  } catch (MalformedURLException ex) {
			Logger.getLogger(RemoteServiceFile.class.getName()).log(Level.SEVERE, null, ex);
		  }
		}
	  });
	} catch (IOException e) {
	  System.err.println("Error occured while parsing remote services");
	  System.err.println(e.getMessage());
	}
  }
  
  

  @Override
  public Set<PlayerCharacter> getOnlineCharacters() {
	
	Set<PlayerCharacter> chars = new HashSet<>();
	
	for (Map.Entry<String,URL> provider : providers.entrySet()) {
	  try (BufferedReader in = new BufferedReader(new InputStreamReader(provider.getValue().openStream()))) {
		String line;
		while ((line = in.readLine()) != null) {
		  if (line.isEmpty())
			continue;
		  PlayerCharacter character = new PlayerCharacter(line);
		  character.setRealm(provider.getKey());
		  chars.add(character);
		}
	  } catch (IOException ex) {
		Logger.getLogger(RemoteServiceFile.class.getName()).log(Level.SEVERE, null, ex);
	  }
	}
	
	return chars;
  }
  
}
