/*
 * The MIT License
 *
 * Copyright 2015 David Knap.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cz.roleplaying.liana;

import cz.roleplaying.liana.model.PlayerCharacter;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 * @author David Knap
 */
public class FriendList {

  private Set<PlayerCharacter> friends;

  public FriendList(Path file) {
	friends = new HashSet<>();
	try (Stream<String> lines = Files.lines(file)) {
	  lines.forEach(s -> friends.add(new PlayerCharacter(s)));
	} catch (IOException e) {
	  System.err.println("Error occured while loading friends from file");
	  System.err.println(e.getMessage());
	}

  }

  public Set<PlayerCharacter> getFriends() {
	return friends;
  }

  boolean isFriend(PlayerCharacter serviceCharacter) {
	for (PlayerCharacter friend : friends) {
	  if (friend.getName().equals(serviceCharacter.getName())) {
		return true;
	  }
	}
	return false;
  }

}
